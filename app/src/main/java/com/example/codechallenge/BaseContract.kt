package com.example.codechallenge


class BaseContract {
    interface Presenter<in T> {
        fun detach()
        fun attach(view: T)
    }

    interface View
}