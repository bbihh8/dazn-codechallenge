package com.example.codechallenge.util

import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val utilModule = module {

    single { Glider(androidApplication()) }

}