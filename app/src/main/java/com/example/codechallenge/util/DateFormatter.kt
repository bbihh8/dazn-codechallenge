package com.example.codechallenge.util

import com.example.codechallenge.CodeChallengeApp
import com.example.codechallenge.R
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter

object DateFormatter {

    fun formatZonedDateTime(dateTime: ZonedDateTime): String {
        val dateNowMidnight = ZonedDateTime.now().withHour(0).withMinute(0).withSecond(0)
        val date = when {
            dateTime.isAfter(dateNowMidnight) && dateTime.isBefore(dateNowMidnight.plusDays(1)) -> {
                CodeChallengeApp.context.getString(R.string.date_today)
            }
            dateTime.isAfter(dateNowMidnight.plusDays(3)) && dateTime.isBefore(dateNowMidnight.plusDays(4)) -> {
                CodeChallengeApp.context.getString(R.string.date_in_three_days)
            }
            dateTime.isAfter(dateNowMidnight.plusDays(2)) && dateTime.isBefore(dateNowMidnight.plusDays(3)) -> {
                CodeChallengeApp.context.getString(R.string.date_in_two_days)
            }
            dateTime.isAfter(dateNowMidnight.plusDays(1)) && dateTime.isBefore(dateNowMidnight.plusDays(2)) -> {
                CodeChallengeApp.context.getString(R.string.date_tomorrow)
            }
            dateTime.isBefore(dateNowMidnight.minusDays(1)) && dateTime.isAfter(dateNowMidnight.minusDays(2)) -> {
                CodeChallengeApp.context.getString(R.string.date_yesterday)
            }
            dateTime.isBefore(dateNowMidnight.minusDays(2)) && dateTime.isAfter(dateNowMidnight.minusDays(3)) -> {
                CodeChallengeApp.context.getString(R.string.date_two_days_ago)
            }
            dateTime.isBefore(dateNowMidnight.minusDays(3)) && dateTime.isAfter(dateNowMidnight.minusDays(4)) -> {
                CodeChallengeApp.context.getString(R.string.date_three_days_ago)
            }
            else -> {
                dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
            }
        }
        return date + dateTime.format(DateTimeFormatter.ofPattern("',' HH:mm"))
    }

}