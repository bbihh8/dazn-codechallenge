package com.example.codechallenge.util

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView

class Glider(context: Context) {

    private val glide: GlideRequests = GlideApp.with(context)
    private val loadedResources: MutableMap<Int, String> = mutableMapOf()

    fun load(
        image: String,
        view: ImageView,
        applier: ((request: GlideRequest<Drawable>) -> Unit)? = null
    ) {
        if (loadedResources[view.id] != image) {
            loadedResources[view.id] = image
        }
        val load = glide.load(loadedResources[view.id])
        applier?.invoke(load)
        load.into(view)
    }

}