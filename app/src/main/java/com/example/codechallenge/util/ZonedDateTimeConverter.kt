package com.example.codechallenge.util

import com.google.gson.*
import io.objectbox.converter.PropertyConverter
import org.threeten.bp.Instant
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.lang.reflect.Type

class ZonedDateTimeConverter : JsonSerializer<ZonedDateTime?>, JsonDeserializer<ZonedDateTime?>,
    PropertyConverter<ZonedDateTime?, String> {

    override fun convertToDatabaseValue(entityProperty: ZonedDateTime?): String {
        if (entityProperty == null) {
            return ""
        }
        return DateTimeFormatter.ISO_INSTANT.format(entityProperty.toInstant())
    }

    override fun convertToEntityProperty(databaseValue: String?): ZonedDateTime? {
        if (databaseValue.isNullOrBlank()) {
            return null
        }
        return try {
            ZonedDateTime.parse(databaseValue, DateTimeFormatter.ISO_INSTANT)
        } catch (e: Throwable) {
            ZonedDateTime.ofInstant(Instant.parse(databaseValue), OffsetDateTime.now().offset)
        }
    }

    override fun serialize(src: ZonedDateTime?, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        return JsonPrimitive(convertToDatabaseValue(src))
    }

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): ZonedDateTime? {
        return convertToEntityProperty(json.asString)
    }

}