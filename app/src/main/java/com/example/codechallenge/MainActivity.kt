package com.example.codechallenge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.codechallenge.ui.playback.PlaybackFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onBackPressed() {
        navHostFragment.getChildFragmentManager().fragments.forEach {
            if (it is PlaybackFragment) {
                it.stopPlaying()
            }
        }

        super.onBackPressed()

    }
}
