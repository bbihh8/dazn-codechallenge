package com.example.codechallenge.api.factory

import com.example.codechallenge.util.ZonedDateTimeConverter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.threeten.bp.ZonedDateTime

object GsonFactory {

    fun createGson(): Gson {
        return GsonBuilder()
            .setLenient()
            .registerTypeAdapter(ZonedDateTime::class.java, ZonedDateTimeConverter())
            .create()
    }

}