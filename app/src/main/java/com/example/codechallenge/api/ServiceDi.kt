package com.example.codechallenge.api

import com.example.codechallenge.api.factory.GsonFactory
import com.example.codechallenge.api.factory.OkHttpClientFactory
import com.example.codechallenge.api.factory.RetrofitFactory
import org.koin.dsl.module
import retrofit2.Retrofit

val serviceModule = module {

    single {
        GsonFactory.createGson()
    }

    single {
        OkHttpClientFactory.createOkHttpClient()
    }

    single {
        RetrofitFactory.createRetrofit(get(), get())
    }

    single<CloudService> { get<Retrofit>().create(CloudService::class.java) }

}