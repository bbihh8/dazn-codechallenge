package com.example.codechallenge.api

import com.example.codechallenge.model.Event
import com.example.codechallenge.model.Schedule
import io.reactivex.Single
import retrofit2.http.GET

interface CloudService {

    @GET("getSchedule")
    fun getSchedule(): Single<List<Schedule>>

    @GET("getEvents")
    fun getEvents(): Single<List<Event>>

}