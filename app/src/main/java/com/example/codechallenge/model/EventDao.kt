package com.example.codechallenge.model

import io.objectbox.Box
import io.objectbox.rx.RxQuery
import io.reactivex.*

class EventDao(val database: Box<Event>) {

    fun observeAllEvents(): Flowable<List<Event>> {
        return RxQuery.observable(database.query().build()).toFlowable(BackpressureStrategy.BUFFER)
            .distinctUntilChanged()
    }

    fun updateAllEvents(events: List<Event>): Completable {
        val toDelete = database.all.filter { dbVal -> events.none { it.id == dbVal.id } }
        database.remove(toDelete)
        return Flowable.fromIterable(events)
            .map { event ->
                val savedEvent: Event? =
                    database.all.firstOrNull { event.id == it.id }
                if (savedEvent == null) {
                    event
                } else {
                    updateExistingEvent(savedEvent, event)
                }
            }
            .toList()
            .flatMap {
                Single.fromCallable {
                    database.put(it)
                    return@fromCallable it
                }
            }
            .ignoreElement()
    }

    private fun updateExistingEvent(savedEvent: Event, response: Event): Event {
        savedEvent.id = response.id
        savedEvent.title = response.title
        savedEvent.subtitle = response.subtitle
        savedEvent.date = response.date
        savedEvent.imageUrl = response.imageUrl
        savedEvent.videoUrl = response.videoUrl
        return savedEvent
    }

}