package com.example.codechallenge.model

import com.example.codechallenge.util.ZonedDateTimeConverter
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import org.threeten.bp.ZonedDateTime

@Entity
data class Schedule (
    @Id var entityId: Long = 0L,
    var id: String = "",
    var title: String = "",
    var subtitle: String = "",
    @Convert(converter = ZonedDateTimeConverter::class, dbType = String::class)
    var date: ZonedDateTime = ZonedDateTime.now(),
    var imageUrl: String = ""
)