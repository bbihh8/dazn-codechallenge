package com.example.codechallenge.model

import io.objectbox.Box
import io.objectbox.rx.RxQuery
import io.reactivex.*

class ScheduleDao (val database: Box<Schedule>) {

    fun observeAllSchedules(): Flowable<List<Schedule>> {
        return RxQuery.observable(database.query().build()).toFlowable(BackpressureStrategy.BUFFER)
            .distinctUntilChanged()
    }

    fun updateAllSchedules(schedules: List<Schedule>): Completable {
        val toDelete = database.all.filter { dbVal -> schedules.none { it.id == dbVal.id } }
        database.remove(toDelete)
        return Flowable.fromIterable(schedules)
            .map { schedule ->
                val savedSchedule: Schedule? =
                    database.all.firstOrNull { schedule.id == it.id }
                if (savedSchedule == null) {
                    schedule
                } else {
                    updateExistingSchedule(savedSchedule, schedule)
                }
            }
            .toList()
            .flatMap {
                Single.fromCallable {
                    database.put(it)
                    return@fromCallable it
                }
            }
            .ignoreElement()
    }

    private fun updateExistingSchedule(savedSchedule: Schedule, response: Schedule): Schedule {
        savedSchedule.id = response.id
        savedSchedule.title = response.title
        savedSchedule.subtitle = response.subtitle
        savedSchedule.date = response.date
        savedSchedule.imageUrl = response.imageUrl
        return savedSchedule
    }

}