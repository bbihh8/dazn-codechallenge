package com.example.codechallenge.model

import io.objectbox.BoxStore
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val daoModule = module {

    single { MyObjectBox.builder().androidContext(androidApplication()).build() }
    single { EventDao(get<BoxStore>().boxFor(Event::class.java)) }
    single { ScheduleDao(get<BoxStore>().boxFor(Schedule::class.java)) }

}