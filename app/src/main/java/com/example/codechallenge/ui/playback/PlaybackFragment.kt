package com.example.codechallenge.ui.playback

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.codechallenge.R
import com.example.codechallenge.util.ExoPlayerHelper
import kotlinx.android.synthetic.main.fragment_playback.*


class PlaybackFragment : Fragment() {

    private lateinit var exoPlayerHelper: ExoPlayerHelper

    companion object {
        const val BUNDLE_URL = "playback_url"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_playback, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (playbackPlayer.player == null) {
            exoPlayerHelper = ExoPlayerHelper(playbackPlayer, {
                Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
            }, {
                if (it) {
                    Log.d("EXO PLAYER", "BUFFERING")
                }
            })
            exoPlayerHelper.initializePlayer(arguments?.getString(BUNDLE_URL) ?: "")
        }
    }

    fun stopPlaying() {
        exoPlayerHelper.killPlayer()
    }

}