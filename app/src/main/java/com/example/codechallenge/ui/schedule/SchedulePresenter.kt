package com.example.codechallenge.ui.schedule

import android.util.Log
import com.example.codechallenge.api.CloudService
import com.example.codechallenge.model.Schedule
import com.example.codechallenge.model.ScheduleDao
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers


class SchedulePresenter(private val cloudService: CloudService, private val scheduleDao: ScheduleDao) : ScheduleContract.Presenter {

    private var view: ScheduleContract.View? = null
    lateinit var disposables: CompositeDisposable

    override fun fetchSchedule() {
        disposables = CompositeDisposable()
        Observable.interval(30, java.util.concurrent.TimeUnit.SECONDS, Schedulers.io())
            .startWith(0L)
            .observeOn(Schedulers.newThread())
            .map { tick -> getSchedule() }
            .doOnError({ error -> Log.e(error.cause?.message, error.message) })
            .retry()
            .subscribe { scheduleObservable ->
                scheduleObservable.subscribeBy {
                    view?.setSchedule(it)
                }
            }.addTo(disposables)
    }

    private fun getSchedule(): Flowable<List<Schedule>> {
        return Flowable.create({ emitter ->
            var error: Throwable? = null
            emitter.setDisposable(disposables)

            cloudService.getSchedule()
                .flatMapCompletable { scheduleDao.updateAllSchedules(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeBy({ error = it })
                .addTo(disposables)

            scheduleDao.observeAllSchedules()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeBy {
                    emitter.onNext(
                        it.sortedWith(compareBy<Schedule> { it.date }
                            .thenBy { it.title }
                        )
                    )
                    error?.let {
                        emitter.onError(it)
                    }
                }
                .addTo(disposables)

        }, BackpressureStrategy.LATEST)
    }

    override fun detach() {
        disposables.clear()
        this.view = null
    }

    override fun attach(view: ScheduleContract.View) {
        this.view = view
    }

}