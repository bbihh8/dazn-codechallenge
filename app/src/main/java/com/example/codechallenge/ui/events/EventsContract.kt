package com.example.codechallenge.ui.events

import com.example.codechallenge.BaseContract
import com.example.codechallenge.model.Event

class EventsContract {

    interface View : BaseContract.View {
        fun setEvents(events: List<Event>)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetchEvents()
    }
}