package com.example.codechallenge.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.codechallenge.R
import com.example.codechallenge.ui.events.EventsFragment
import com.example.codechallenge.ui.schedule.ScheduleFragment
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        switchFragment(EventsFragment.newInstance(), EventsFragment.TAG)

        homeBottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.getItemId()) {
                R.id.navigationEvents -> {
                    switchFragment(EventsFragment.newInstance(), EventsFragment.TAG)
                }
                R.id.navigationSchedule -> {
                    switchFragment(ScheduleFragment.newInstance(), ScheduleFragment.TAG)
                }
            }
            true
        }
    }

    private fun switchFragment(fragment: Fragment, tag: String) {
        val actualFragment = activity?.getSupportFragmentManager()?.findFragmentByTag(tag)
        if (actualFragment == null || !actualFragment.isVisible()) {
            activity?.getSupportFragmentManager()
                ?.beginTransaction()
                ?.replace(R.id.homeFragmentContainer, fragment, tag)
                ?.commit()
        }
    }

}