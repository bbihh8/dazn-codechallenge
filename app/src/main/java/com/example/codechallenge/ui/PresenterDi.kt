package com.example.codechallenge.ui

import com.example.codechallenge.ui.events.EventsContract
import com.example.codechallenge.ui.events.EventsPresenter
import com.example.codechallenge.ui.schedule.ScheduleContract
import com.example.codechallenge.ui.schedule.SchedulePresenter
import org.koin.dsl.module

val presenterModule = module {

    single<ScheduleContract.Presenter> { SchedulePresenter(get(), get()) }
    single<EventsContract.Presenter> { EventsPresenter(get(), get()) }

}