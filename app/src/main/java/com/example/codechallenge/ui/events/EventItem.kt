package com.example.codechallenge.ui.events

import android.view.View
import com.example.codechallenge.model.Event
import com.example.codechallenge.util.DateFormatter
import com.example.codechallenge.util.Glider
import com.example.codechallenge.util.SimpleAbstractItem
import com.mikepenz.fastadapter.diff.DiffCallback
import kotlinx.android.synthetic.main.item_event.view.*


class EventItem(
    val event: Event,
    private val glider: Glider,
    val action: () -> Unit
) : SimpleAbstractItem() {

    override fun bind(view: View) {

        view.eventItemName.text = event.title
        view.eventItemDate.text = DateFormatter.formatZonedDateTime(event.date)
        view.eventItemDescription.text = event.subtitle

        glider.load(event.imageUrl, view.eventItemImage) {
            it.centerCrop()
        }
        view.setOnClickListener { action() }
    }

    override val layoutRes: Int = com.example.codechallenge.R.layout.item_event

    class EventDiffCallback : DiffCallback<EventItem> {
        override fun areItemsTheSame(oldItem: EventItem, newItem: EventItem): Boolean {
            return true
        }

        override fun areContentsTheSame(oldItem: EventItem, newItem: EventItem): Boolean {
            return oldItem.event.id == newItem.event.id
        }

        override fun getChangePayload(
            oldItem: EventItem,
            oldItemPosition: Int,
            newItem: EventItem,
            newItemPosition: Int
        ): Any? {
            return null
        }
    }
}