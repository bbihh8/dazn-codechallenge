package com.example.codechallenge.ui.schedule

import com.example.codechallenge.BaseContract
import com.example.codechallenge.model.Schedule

class ScheduleContract {

    interface View : BaseContract.View {
        fun setSchedule(schedule: List<Schedule>)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun fetchSchedule()
    }

}