package com.example.codechallenge.ui.schedule

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.codechallenge.R
import com.example.codechallenge.model.Schedule
import com.example.codechallenge.util.Glider
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import kotlinx.android.synthetic.main.fragment_schedule.*
import org.koin.android.ext.android.inject

class ScheduleFragment : Fragment(), ScheduleContract.View {
    private val adapter = FastItemAdapter<ScheduleItem>()
    private val glider: Glider by inject()
    private val presenter: ScheduleContract.Presenter by inject()

    companion object {
        fun newInstance() = ScheduleFragment()
        val TAG = "schedule_fragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_schedule, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        scheduleRecycler.adapter = adapter
        val itemDecorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        ContextCompat.getDrawable(context!!, R.drawable.recycler_divider)?.let {
            itemDecorator.setDrawable(it)
            scheduleRecycler.addItemDecoration(itemDecorator)
        }

        activity?.apply {
            presenter.attach(this@ScheduleFragment)
            presenter.fetchSchedule()
        }
    }

    override fun setSchedule(schedule: List<Schedule>) {
        FastAdapterDiffUtil.set(
            adapter.itemAdapter,
            schedule.map { ScheduleItem(it, glider) },
            ScheduleItem.ScheduleDiffCallback()
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

}