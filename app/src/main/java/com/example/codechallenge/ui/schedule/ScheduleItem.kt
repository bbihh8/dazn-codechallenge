package com.example.codechallenge.ui.schedule

import android.view.View
import com.example.codechallenge.R
import com.example.codechallenge.model.Schedule
import com.example.codechallenge.util.DateFormatter
import com.example.codechallenge.util.Glider
import com.example.codechallenge.util.SimpleAbstractItem
import com.mikepenz.fastadapter.diff.DiffCallback
import kotlinx.android.synthetic.main.item_event.view.*

class ScheduleItem (
    val schedule: Schedule,
    private val glider: Glider
) : SimpleAbstractItem() {

    override fun bind(view: View) {
        view.eventItemName.text = schedule.title
        view.eventItemDescription.text = schedule.subtitle
        view.eventItemDate.text = DateFormatter.formatZonedDateTime(schedule.date)

        glider.load(schedule.imageUrl, view.eventItemImage) {
                it.centerCrop()
        }

    }

    override val layoutRes: Int = R.layout.item_event

    class ScheduleDiffCallback : DiffCallback<ScheduleItem> {
        override fun areItemsTheSame(oldItem: ScheduleItem, newItem: ScheduleItem): Boolean {
            return true
        }

        override fun areContentsTheSame(oldItem: ScheduleItem, newItem: ScheduleItem): Boolean {
            return oldItem.schedule.id == newItem.schedule.id
        }

        override fun getChangePayload(
            oldItem: ScheduleItem,
            oldItemPosition: Int,
            newItem: ScheduleItem,
            newItemPosition: Int
        ): Any? {
            return null
        }
    }
}