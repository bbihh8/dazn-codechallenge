package com.example.codechallenge.ui.events

import com.example.codechallenge.api.CloudService
import com.example.codechallenge.model.Event
import com.example.codechallenge.model.EventDao
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers


class EventsPresenter(private val cloudService: CloudService, private val eventDao: EventDao) :
    EventsContract.Presenter {

    private var view: EventsContract.View? = null
    lateinit var disposables: CompositeDisposable

    override fun fetchEvents() {
        Flowable.create<List<Event>>({ emitter ->
            var error: Throwable? = null
            emitter.setDisposable(disposables)

            cloudService.getEvents()
                .flatMapCompletable { eventDao.updateAllEvents(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeBy({ error = it })
                .addTo(disposables)

            eventDao.observeAllEvents()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeBy {
                    emitter.onNext(
                        it.sortedWith(compareBy<Event> { it.date }
                            .thenBy { it.title }
                        )
                    )
                    error?.let {
                        emitter.onError(it)
                    }
                }
                .addTo(disposables)

        }, BackpressureStrategy.LATEST).subscribeBy {
            view?.setEvents(it)
        }.addTo(disposables)

    }

    override fun detach() {
        disposables.clear()
        this.view = null
    }

    override fun attach(view: EventsContract.View) {
        this.view = view
        disposables = CompositeDisposable()
    }

}