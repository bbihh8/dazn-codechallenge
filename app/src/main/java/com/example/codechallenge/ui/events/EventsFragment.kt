package com.example.codechallenge.ui.events

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.codechallenge.R
import com.example.codechallenge.model.Event
import com.example.codechallenge.ui.playback.PlaybackFragment
import com.example.codechallenge.util.Glider
import com.mikepenz.fastadapter.adapters.FastItemAdapter
import com.mikepenz.fastadapter.diff.FastAdapterDiffUtil
import kotlinx.android.synthetic.main.fragment_events.*
import org.koin.android.ext.android.inject


class EventsFragment : Fragment(), EventsContract.View {
    private val adapter = FastItemAdapter<EventItem>()
    private val glider: Glider by inject()
    private val presenter: EventsContract.Presenter by inject()

    companion object {
        fun newInstance() = EventsFragment()
        val TAG = "events_fragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_events, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventsRecycler.adapter = adapter
        val itemDecorator = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        ContextCompat.getDrawable(context!!, R.drawable.recycler_divider)?.let {
            itemDecorator.setDrawable(it)
            eventsRecycler.addItemDecoration(itemDecorator)
        }

        activity?.apply {
            presenter.attach(this@EventsFragment)
            presenter.fetchEvents()
        }

    }

    override fun setEvents(events: List<Event>) {
        FastAdapterDiffUtil.set(
            adapter.itemAdapter,
            events.map {
                EventItem(it, glider, {
                    val args = Bundle()
                    args.putString(PlaybackFragment.BUNDLE_URL, it.videoUrl)
                    activity?.findNavController(R.id.navHostFragment)
                        ?.navigate(R.id.action_homeFragment_to_playbackFragment, args)
                })
            },
            EventItem.EventDiffCallback()
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach()
    }

}