package com.example.codechallenge

import android.app.Application
import com.example.codechallenge.api.serviceModule
import com.example.codechallenge.model.daoModule
import com.example.codechallenge.ui.presenterModule
import com.example.codechallenge.util.utilModule
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CodeChallengeApp : Application() {

    companion object {
        lateinit var context: CodeChallengeApp private set
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        startKoin {
            androidContext(this@CodeChallengeApp)
            modules(
                listOf(
                    serviceModule,
                    daoModule,
                    presenterModule,
                    utilModule
                )
            )
        }
        AndroidThreeTen.init(this)
    }

}