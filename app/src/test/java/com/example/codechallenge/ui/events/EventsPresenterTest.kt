package com.example.codechallenge.ui.events

import android.accounts.NetworkErrorException
import com.example.codechallenge.BaseTest
import com.example.codechallenge.api.CloudService
import com.example.codechallenge.model.Event
import com.example.codechallenge.model.EventDao
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.threeten.bp.ZonedDateTime
import java.io.IOException


@RunWith(MockitoJUnitRunner::class)
class EventsPresenterTest: BaseTest() {

    @Mock
    private lateinit var view: EventsContract.View

    lateinit var presenter: EventsPresenter

    private val dao: EventDao = mock()
    private val cloudService: CloudService = mock()

    private val events = listOf(
        Event(
            id = "1",
            title = "Liverpool v Porto",
            subtitle = "Champions League",
            date = ZonedDateTime.now().minusDays(1),
            imageUrl = "image.jpg",
            videoUrl = "video.mp4"
        )
    )

    @Before
    fun init() {
        whenever(cloudService.getEvents()).thenReturn(Single.just(events))
        whenever(dao.observeAllEvents()).thenReturn(Flowable.just(events))
        whenever(dao.updateAllEvents(any())).thenReturn(Completable.complete())

        presenter = EventsPresenter(cloudService, dao)
        presenter.attach(view)
    }

    @Test
    fun `fetch events should return positive response`() {
        presenter.fetchEvents()
        Mockito.verify(view).setEvents(events)
    }

    @Test
    fun `fetch events should return negative response when api return error`() {
        given(cloudService.getEvents()).willAnswer {
            throw NetworkErrorException("500")
        }
        presenter.fetchEvents()
        Mockito.verify(view, never()).setEvents(events)
    }

    @Test
    fun `fetch events should return negative response when dao return error`() {
        given(dao.observeAllEvents()).willAnswer {
            throw IOException("DAO Exception")
        }
        presenter.fetchEvents()
        Mockito.verify(view, never()).setEvents(events)
    }

}