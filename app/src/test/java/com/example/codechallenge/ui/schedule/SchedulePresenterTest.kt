package com.example.codechallenge.ui.schedule

import android.accounts.NetworkErrorException
import com.example.codechallenge.BaseTest
import com.example.codechallenge.api.CloudService
import com.example.codechallenge.model.Schedule
import com.example.codechallenge.model.ScheduleDao
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.threeten.bp.ZonedDateTime
import java.io.IOException
import java.util.concurrent.TimeUnit


@RunWith(MockitoJUnitRunner::class)
class SchedulePresenterTest: BaseTest() {

    @Mock
    private lateinit var view: ScheduleContract.View

    lateinit var presenter: SchedulePresenter
    lateinit var testScheduler: TestScheduler

    private val dao: ScheduleDao = mock()
    private val cloudService: CloudService = mock()

    private val schedules = listOf(
        Schedule(
            id = "1",
            title = "Liverpool v Porto",
            subtitle = "Champions League",
            date = ZonedDateTime.now().minusDays(1),
            imageUrl = "image.jpg"
        )
    )

    @Before
    fun init() {
        RxJavaPlugins.reset()
        testScheduler = TestScheduler()
        RxJavaPlugins.setComputationSchedulerHandler { testScheduler }
        RxJavaPlugins.setNewThreadSchedulerHandler { testScheduler }
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
        Mockito.reset(dao, cloudService, view)
        presenter = SchedulePresenter(cloudService, dao)
        presenter.attach(view)
    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
    }

    @Test
    fun `fetch schedules should return positive response`() {
        whenever(cloudService.getSchedule()).thenReturn(Single.just(schedules))
        whenever(dao.observeAllSchedules()).thenReturn(Flowable.just(schedules))
        whenever(dao.updateAllSchedules(any())).thenReturn(Completable.complete())

        presenter.fetchSchedule()
        testScheduler.advanceTimeTo(30, TimeUnit.SECONDS)

        Mockito.verify(view, times(2)).setSchedule(schedules)
    }

    @Test
    fun `fetch schedules should return negative response when api return error`() {
        BDDMockito.given(cloudService.getSchedule()).willAnswer {
            throw NetworkErrorException("500")
        }

        presenter.fetchSchedule()
        testScheduler.advanceTimeTo(30, TimeUnit.SECONDS)

        Mockito.verify(view, never()).setSchedule(schedules)
    }

    @Test
    fun `fetch schedules should return negative response when dao return error`() {
        whenever(cloudService.getSchedule()).thenReturn(Single.just(schedules))
        BDDMockito.given(dao.observeAllSchedules()).willAnswer {
            throw IOException("DAO Exception")
        }

        presenter.fetchSchedule()
        testScheduler.advanceTimeTo(30, TimeUnit.SECONDS)

        Mockito.verify(view, never()).setSchedule(schedules)
    }

}