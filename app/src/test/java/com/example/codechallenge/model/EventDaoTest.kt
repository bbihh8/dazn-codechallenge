package com.example.codechallenge.model

import com.example.codechallenge.BaseDaoTest
import io.objectbox.Box
import org.junit.Test
import org.threeten.bp.ZonedDateTime

class EventDaoTest  : BaseDaoTest() {

    lateinit var dao: EventDao
    lateinit var db: Box<Event>

    val event = Event(
        id = "1",
        title = "Liverpool v Porto",
        subtitle = "Champions League",
        date = ZonedDateTime.now().minusDays(1),
        imageUrl = "image.jpg",
        videoUrl = "video.mp4"
    )
    val beforeUpdateEvent = Event(
        id = "1",
        title = "Liverpool v Porto",
        subtitle = "Champions League",
        date = ZonedDateTime.now().minusDays(4),
        imageUrl = "image222.jpg",
        videoUrl = "video222.mp4"
    )
    val events = listOf(event)

    override fun before() {
        db = store!!.boxFor(Event::class.java)
        dao = EventDao(db)
    }

    @Test
    fun `should return empty list when event is not present in db`() {
        dao.observeAllEvents()
            .test()
            .awaitCount(1)
            .assertValueAt(0) { it.isEmpty() }
            .assertNotComplete()
    }

    @Test
    fun `should return list when event is saved in db`() {
        db.put(events)

        dao.observeAllEvents()
            .test()
            .awaitCount(1)
            .assertValueAt(0) { it.isNotEmpty() }
            .assertValueCount(1)

    }

    @Test
    fun `should update event when an event with the same id is in db`() {
        db.put(beforeUpdateEvent)

        dao.updateAllEvents(events)
            .test()
            .await()
            .assertComplete()

        val events = dao.observeAllEvents().blockingFirst()

        assert(events.size == 1)
        assert(events.first().id == event.id)
        assert(events.first().title == event.title)
        assert(events.first().subtitle == event.subtitle)
        assert(events.first().imageUrl == event.imageUrl)
        assert(events.first().date.isEqual(event.date))
    }


}