package com.example.codechallenge.model

import com.example.codechallenge.BaseDaoTest
import io.objectbox.Box
import org.junit.Test
import org.threeten.bp.ZonedDateTime

class ScheduleDaoTest : BaseDaoTest() {

    lateinit var dao: ScheduleDao
    lateinit var db: Box<Schedule>

    val schedule = Schedule(
        id = "1",
        title = "Liverpool v Porto",
        subtitle = "Champions League",
        date = ZonedDateTime.now().minusDays(1),
        imageUrl = "image.jpg"
    )
    val beforeUpdateSchedule = Schedule(
        id = "1",
        title = "Liverpool v Porto",
        subtitle = "Champions League",
        date = ZonedDateTime.now().minusDays(4),
        imageUrl = "image222.jpg"
    )
    val schedules = listOf(schedule)

    override fun before() {
        db = store!!.boxFor(Schedule::class.java)
        dao = ScheduleDao(db)
    }

    @Test
    fun `should return empty list when schedule is not present in db`() {
        dao.observeAllSchedules()
            .test()
            .awaitCount(1)
            .assertValueAt(0) { it.isEmpty() }
            .assertNotComplete()
    }

    @Test
    fun `should return list when schedule is saved in db`() {
        db.put(schedule)

        dao.observeAllSchedules()
            .test()
            .awaitCount(1)
            .assertValueAt(0) { it.isNotEmpty() }
            .assertValueCount(1)

    }

    @Test
    fun `should update schedule when a schedule with the same id is in db`() {
        db.put(beforeUpdateSchedule)

        dao.updateAllSchedules(schedules)
            .test()
            .await()
            .assertComplete()

        val schedules = dao.observeAllSchedules().blockingFirst()

        assert(schedules.size == 1)
        assert(schedules.first().id == schedule.id)
        assert(schedules.first().title == schedule.title)
        assert(schedules.first().subtitle == schedule.subtitle)
        assert(schedules.first().imageUrl == schedule.imageUrl)
        assert(schedules.first().date.isEqual(schedule.date))
    }


}